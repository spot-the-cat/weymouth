/**
 * Filter the companies on the about page, (I'm not a JavaScript coder, my humble apologies).
 */
jQuery213(function() {

// Init Isotope

  var grid = jQuery213('.grid').isotope({
    itemSelector: '.element-item',
    layoutMode: 'fitRows'
  });
// Bind filter button click.

  jQuery213('.filters-button-group').on('click', 'button', function() {
    var filterValue = jQuery213(this).attr('data-filter');
    grid.isotope({filter: filterValue});
  });
// Change is-checked class on buttons.

  jQuery213('.button-group').each(function(i, buttonGroup) {
    var buttonGroup = jQuery213(buttonGroup);

    buttonGroup.on('click', 'button', function() {
      buttonGroup.find('.is-checked').removeClass('is-checked');
      jQuery213(this).addClass('is-checked');
    });
  });
});
