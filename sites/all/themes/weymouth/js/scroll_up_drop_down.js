/**
 * Display the header when scrolling up.
 */
jQuery(window).load(function () {
  'use strict';
  var lastScrollTop = 0;
  var thisScrollTop;
  var winHeight = Math.floor(jQuery(window).height());

// Display the navigation header when scrolling up.

  jQuery(window).bind('scroll', function() {
    var dir = detectDirection();
    var pos = jQuery(window).scrollTop();
    
// When we're half way above the fold show the normal menu icon and hide the nav header.

    if (pos < winHeight/2) {
      jQuery('.nav-drawer').removeClass('show');
      jQuery('#off-canvas-show').css('display', 'block');
      return;
    }
// When we're half way to the fold hide everthing.

    else if (winHeight * 0.5 <= pos && pos < winHeight * 0.6) {
      jQuery('.nav-drawer').removeClass('show');
      jQuery('#off-canvas-show').css('display', 'none');
      return;
    }
// If we're scrolling down and we're below the fold hide the header nav.
    
    else if (dir == 'down') {
      jQuery('.nav-drawer').removeClass('show');
    }
// If we're scrolling up and we're below the fold show the header nav.
    
    else {
      jQuery('.nav-drawer').addClass('show');
    }    
  });
/**
 * Get the scroll direction.
 */
  function detectDirection() {
    thisScrollTop = window.pageYOffset;    
    var direction = (thisScrollTop > lastScrollTop)? 'down': 'up';
    lastScrollTop = thisScrollTop;    
    return  direction;
  }
});
