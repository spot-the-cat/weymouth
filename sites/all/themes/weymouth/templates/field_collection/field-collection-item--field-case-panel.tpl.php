<?php

/**
 * @file field-collection-item--field-case-panel.tpl.php
 * Template for the field_case_panel field collection.
 *
 * Available variables:
 * - $content (render array)
 * - - field_case_panel_body
 * - - field_case_panel_title
 * - - field_tax_dongle
 * - - field_case_panel_img
 */
$dongle = trim(render($content['field_tax_dongle']));
?>
<div class="case-panel">
  <div class="case-panel-txt">
    
  <?php if (!empty($content['field_case_panel_title'])): ?>
    <h2 class="case-panel-title"><i class="fa fa-caret-left"></i><?php print render($content['field_case_panel_title']); ?></h2>
  <?php endif; ?>
    
    <?php print render($content['field_case_panel_body']); ?>
  </div>

  <?php if ($dongle == 'slideshow'): ?>
    <?php print theme('weymouth_core_case_slideshow', array('content' => $content)); ?>
  <?php else: ?>
    <?php print theme('weymouth_core_case_dongle', array('content' => $content)); ?>
  <?php endif; ?>

</div>
<div class="clear-l">
</div>
