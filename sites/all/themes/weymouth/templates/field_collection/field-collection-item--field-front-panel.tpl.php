<?php

/**
 * @file field-collection-item--field-front-panel.tpl.php
 * Template for the field_front_panel field collection.
 *
 * Available variables:
 * - $content (render array)
 * - - field_front_panel_body
 * - - field_front_panel_img
 * - - field_front_panel_ref
 * - - field_tax_market
 */
?>
<div class="front-panel">
  <?php print render($content['field_front_panel_body']); ?>
  <div class="front-panel-content" style="background:url(<?php print render($content['field_front_panel_img']); ?>) no-repeat center center / cover">

    <div class="front-panel-callout">
      
    <?php if (!empty($content['field_tax_market'])): ?>
      <h4><?php print render($content['field_tax_market']); ?></h4>
    <?php endif; ?>
      
      <?php print render($content['field_front_panel_ref']); ?>
    </div>
  </div>
</div>

