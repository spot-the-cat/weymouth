<?php

/**
 * @file field-collection-item--field-front-video.tpl.php
 * Template for the field_front_video field collection.
 *
 * Available variables:
 * - $content (render array)
 * - - field_front_video_url
 * - - field_front_video_img
 */
?>
<video width="100%" class="video-js vjs-default-skin" autoplay preload="auto" data-setup="{}">
  <source src="<?php print render($content['field_front_video_url']); ?>" poster="<?php print render($content['field_front_video_img']); ?>" type="video/mp4" />
</video>
<img src="<?php print render($content['field_front_video_img']); ?>" class="front-mobile-img" />
<a href="#case-study" class="explore"><div>Explore</div><i class="fa fa-chevron-down"></i></a>

