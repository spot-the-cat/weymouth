<?php

/**
 * @file weymouth-core-global-footer.tpl.php
 * Template for the global footer block.
 *
 * Available variables:
 * - $data (object)
 * - - site_name (string)
 * - - facebook_url (string)
 * - - linkedin_url (string)
 * - - twitter_url (string)
 * - - vimeo_url (string)
 * - - logo (object)
 * - - - height (string)
 * - - - width (string)
 * - - - url (string)
 * - - offices (array of objects)
 * - - - title (string)
 * - - - email (string)
 * - - - map (string)
 * - - - phone (string)
 * - - - street (string)
 */
?>
<div id="mobile-contact" class="mobile">
  
  <?php foreach ($data->offices as $i => $office): ?>
    <div id="mobile-office-<?php print $i; ?>" class="mobile-office">
      <div class="mobile-office-contact">
        <h2><?php print $office->title; ?></h2>
        <?php print l('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-phone fa-stack-1x fa-inverse"></i></span>', $office->phone, array('html' => TRUE)); ?>
        <?php print l('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span>', $office->email, array('html' => TRUE)); ?>
        <?php print $office->circle; ?>
      </div>
    </div>
  <?php endforeach; ?>

</div>
