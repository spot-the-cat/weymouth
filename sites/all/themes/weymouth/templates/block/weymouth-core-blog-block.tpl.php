<?php

/**
 * @file weymouth-core-blog-block.tpl.php
 * Template for the blog block.
 *
 * Available variables:
 * - $data (object)
 * - - url (string)
 * - - img (string)
 * - - blogs (array of objects)
 * - - - nid (int)
 * - - - content (string)
 */
?>
<ul id="blog-block">

<?php foreach ($data->blogs as $i => $obj): ?>
  <li id="blog-<?php print $obj->nid; ?>" class="blog-block blog-block-<?php print $i; ?> <?php print ($i % 2)? 'odd': 'even'; ?>"><?php print $obj->content; ?></li>
<?php endforeach; ?>

  <li class="blog-block blog-block-<?php print ($i + 1); ?> <?php print ($i % 2)? 'even': 'odd'; ?> blog-block-last">
    <a href="<?php print $data->url; ?>" target="_blank">
      <img src="/<?php print $data->img; ?>">
      <h2 class="title node-title blog-teaser-title">Trends. Opinions. Insights.</h2>
      <h4>Read More</h4>
    </a>
  </li>
</ul>
