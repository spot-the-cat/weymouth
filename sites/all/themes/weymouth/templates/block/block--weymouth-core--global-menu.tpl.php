<?php

/**
 * @file
 * Template for the global menu block.
 *
 * Available variables:
 * - $content
 */
?>
<div id="global-menu">
  <div class="content">
    <?php print $content; ?>
  </div>
</div>
