<?php

/**
 * @file weymouth-core-global-footer.tpl.php
 * Template for the global footer block.
 *
 * Available variables:
 * - $data (object)
 * - - site_name (string)
 * - - facebook_url (string)
 * - - linkedin_url (string)
 * - - twitter_url (string)
 * - - vimeo_url (string)
 * - - logo (object)
 * - - - height (string)
 * - - - width (string)
 * - - - url (string)
 * - - offices (array of objects)
 * - - - title (string)
 * - - - email (string)
 * - - - map (string)
 * - - - phone (string)
 * - - - street (string)
 */
?>
<div id="global-footer">
  <div id="global-footer-left" class="global-footer-section">
    <a href="/">
      <img src="<?php print $data->logo->url; ?>" width="<?php print $data->logo->width; ?>" height="<?php print $data->logo->height; ?>" alt="<?php print $data->site_name; ?>">
      <h3><?php print $data->site_name; ?></h3>
    </a>
  </div>
  <div id="global-footer-center" class="global-footer-section">

  <?php foreach ($data->offices as $office): ?>
    <div class="footer-office">
      <?php print $office->map; ?>
      <?php print $office->title; ?><br />
      <?php print $office->street; ?><br />
      <span class="footer-office-phone"><?php print $office->phone; ?></span><br />
      <?php print l($office->email, "mailto:$office->email"); ?>
    </div>
  <?php endforeach; ?>

  </div>
  <div id="global-footer-right" class="global-footer-section">
    <?php print l('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span>', $data->facebook_url, array('absolute' => TRUE, 'html' => TRUE)); ?>
    <?php print l('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></span>', $data->twitter_url, array('absolute' => TRUE, 'html' => TRUE)); ?>
    <?php print l('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x fa-inverse"></i></span>', $data->linkedin_url, array('absolute' => TRUE, 'html' => TRUE)); ?>
    <?php print l('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-vimeo-square fa-stack-1x fa-inverse"></i></span>', $data->vimeo_url, array('absolute' => TRUE, 'html' => TRUE)); ?>
  </div>
  <div class="clear">
  </div>
</div>


