<?php

/**
 * @file weymouth-core-client.tpl.php
 * The theme template of for a client list page.
 *
 * Available variables:
 * - $data (object)
 * - - title (string)
 * - - term (string)
 * - - nid (int)
 * - - tid (int)
 * - - terms (array)
 * - - slides (array of objects)
 * - - - type (string)
 * - - - value (string)
 * - - - data (object) 
 */
?>
<div id="client">
  <ul id="client-services" class="links links--inline node__links">
    <li><a href="<?php print url("node/$data->nid"); ?>"><img src="/<?php print path_to_theme(); ?>/images/client-eye.png" width="40" height="30"></a></li>
    
  <?php foreach ($data->terms as $tid => $term): ?>
    <li id="client-service-<?php print $tid; ?>"><?php print l($term, "client/$data->nid/$tid"); ?></li>
  <?php endforeach; ?>
  
  </ul>
  <div class="clear-r">
  </div>
  <div class="flexslider">
    <ul id="client-slides" class="slides">
      
    <?php foreach ($data->slides as $i => $obj): ?>
      <li id="client-slide-<?php print $i; ?>"><?php print $obj->value; ?></li>
    <?php endforeach; ?>
  
    </ul>
  </div>
</div>
