<?php
/**
 * @file
 * Default theme implementation to display a video embed field.
 *
 * Variables available:
 * - $url: The URL of the video to display embed code for
 * - $style: The style name of the embed code to use
 * - $style_settings: The style settings for the embed code
 * - $handler: The name of the video handler
 * - $embed_code: The embed code
 * - $data: Additional video data
 *
 * @see template_preprocess_video_embed()
 */
?>
<?php if (!empty($data->id)): ?>
<div class="embedded-video">
  <div class="player">
    <iframe src="https://player.vimeo.com/video/<?php print $data->id; ?>" width="300" height="185" class="phone" style="display:none" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    <iframe src="https://player.vimeo.com/video/<?php print $data->id; ?>" width="650" height="401" class="tab" style="display:none" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    <iframe src="https://player.vimeo.com/video/<?php print $data->id; ?>" width="890" height="550" class="desk" style="display:none" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
  </div>
</div>
<?php endif; ?>
