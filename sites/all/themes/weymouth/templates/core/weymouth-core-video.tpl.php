<?php

/**
 * @file weymouth-core-video.tpl.php
 * Template for the front page AJAX video callback.
 *
 * Available variables:
 * - $data (object)
 * - - url
 * - - img
 */
?>
<div class="video-wrapper">
  <video width="100%" class="video-js vjs-default-skin" style="position:absolute; top:0; width:100%" autoplay preload="auto" data-setup="{}">
    <source src="<?php print $data->url; ?>" poster="<?php print $data->img; ?>" type="video/mp4" />
  </video>
  <img src="<?php print $data->img; ?>" class="front-mobile-img" />
  <a href="#case-study" class="explore-ajax"><div>Explore</div><i class="fa fa-chevron-down"></i></a>
  <a href="/video/27/0/nojs" id="video-x" class="use-ajax">&times;</a>
</div>
