<?php

/**
 * @file weymouth-core-markets.tpl.php
 * The theme template of for the industries page.
 *
 * Available variables:
 * - $data - An array of objects with the following properties.
 */
?>
<?php foreach ($data as $obj): ?>
  <a href="<?php print url("industries/$obj->tid"); ?>">
    <div<?php print !empty($obj->img->url)? " style=\"background:url({$obj->img->url}) no-repeat center center; background-size:cover; height:20em; margin-top:-2em\"": ''; ?>>
      <h2 style="color:#ffffff; font-size:2em; left:25%; position:relative; text-align:center; top:45%; width:50%"><?php print $obj->title; ?></h2>
    </div>
  </a>
<?php endforeach; ?>
