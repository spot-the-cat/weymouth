<?php

/**
 * @file weymouth-core-cases.tpl.php
 * The theme template of for the case-studies page.
 *
 * Available variables:
 * - $data - An array of objects with the following properties.
 */
?>
<ul id="case-studies">
  
<?php foreach ($data as $obj): ?>
  <li><?php print $obj->content; ?></li>
<?php endforeach; ?>

</ul>
