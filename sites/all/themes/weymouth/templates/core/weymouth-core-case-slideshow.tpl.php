<?php

/**
 * @file weymouth-core-case-slideshow.tpl.php
 * Template to display the image fields in the case_panel field collection as a slideshow.
 *
 * Available variables:
 * - $content (render array)
 * - - field_case_panel_img (array)
 */
?>
<div class="flexslider">
  <ul id="case-slides" class="slides case-panel-slideshow">

<?php for ($i = 0; !empty($content['field_case_panel_fluf'][$i]); $i++): ?>
  <li id="case-panel-img-<?php print $i; ?>" class="case-panel-img"><?php print render($content['field_case_panel_fluf'][$i]); ?></li>
<?php endfor; ?>

  </ul>
</div>