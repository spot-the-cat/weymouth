<?php

/**
 * @file node--project.tpl.php
 * Template for the teaser display of the project content type.
 *
 * Available variables:
 * - $title (string)
 * - $content (render array)
 * - $node (object)
 */
?>
<article id="node-<?php print $node->nid; ?>" class="node node-project node-project-teaser teaser">
  <div class="content">
    <?php print render($content['field_project_img'][0]); ?>
    <h2 class="title node-title"><?php print $title; ?></h2>
  </div>
</article>



