<?php

/**
 * @file
 * Template for the webform content type.
 *
 * Available variables:
 * - $title (string)
 * - $content (render array)
 * - $node (object)
 */
// We hide the comments and links now so that we can render them later.

hide($content['comments']);
hide($content['links']);
?>
<div id="webform-contact">
  <?php print render($content); ?>
</div>
