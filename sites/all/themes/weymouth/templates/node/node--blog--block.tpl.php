<?php

/**
 * @file node--blog.tpl.php
 * Default template for the blog content type.
 *
 * Available variables:
 * - $title (string)
 * - $content (render array)
 * - - body
 * - - field_blog_img
 * - - field_blog_url
 * - $node (object)
 * - $view_mode (string)
 */
?>
<a href="<?php print trim(render($content['field_blog_url'])); ?>" target="_blank">
  <article id="node-<?php print $node->nid; ?>" class="node node-blog node-blog-teaser teaser">
    <div class="content">
      <?php print render($content['field_blog_img']); ?>
      <h2 class="title node-title blog-teaser-title"><?php print $title; ?></h2>
    </div>
  </article>
</a>
