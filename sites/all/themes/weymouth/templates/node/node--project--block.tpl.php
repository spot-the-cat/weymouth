<?php

/**
 * @file node--project.tpl.php
 * Template for the block display of the project content type.
 *
 * Available variables:
 * - $title (string)
 * - $content (render array)
 * - $node (object)
 * - $uid (int)
 * - $view_mode (string)
 * - $is_front (boolean)
 * - $logged_in (boolean)
 */
// We hide the comments and links now so that we can render them later.

hide($content['comments']);
hide($content['links']);
?>
<article id="node-<?php print $node->nid; ?>" class="node node-project node-project-<?php print $view_mode; ?> <?php print $view_mode; ?>">
  <?php if (!$page): ?>
    <header>
        <h2 class="title node-title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    </header>
  <?php endif; ?>

  <div class="content">
    <?php print render($content); ?>
  </div>
</article>
