<?php

/**
 * @file node--project.tpl.php
 * Template for the full display of the project content type.
 *
 * Available variables:
 * - $title (string)
 * - $content (render array)
 * - - body
 * - - field_project_facet
 * - - field_project_img
 * - - field_project_url
 * - - field_tax_market
 * - - field_tax_service
 * - $node (object)
 */
?>
<article id="node-<?php print $node->nid; ?>" class="node node-project node-project-full full">
  <div class="content">
    <?php print render($content['field_project_img']); ?>
    <?php print render($content['links']); ?>
    <?php print render($content['body']); ?>
  </div>
</article>
