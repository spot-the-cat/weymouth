<?php

/**
 * @file node--blog.tpl.php
 * Default template for the blog content type.
 *
 * Available variables:
 * - $title (string)
 * - $content (render array)
 * - - body
 * - - field_blog_img
 * - - field_blog_url
 * - $node (object)
 * - $view_mode (string)
 */
?>
<article id="node-<?php print $node->nid; ?>" class="node node-blog node-blog-<?php print $view_mode; ?> <?php print $view_mode; ?>">
  
<?php if (!$page): ?>
  <header>
    <h2 class="title node-title"><a href="<?php print $node_url; ?>" rel="bookmark"><?php print $title; ?></a></h2>
  </header>
<?php endif; ?>

  <div class="content">
    <?php print render($content['field_blog_img']); ?>
    <?php print render($content['body']); ?>
    <a href="<?php print render($content['field_blog_url']); ?>" target="_blank">Read More &raquo;</a>
  </div>
</article>
