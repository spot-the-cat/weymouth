<?php

/**
 * @file node.tpl.php
 * Template for the full display of the front content type.
 *
 * Available variables:
 * - $title (string)
 * - $content (render array)
 * - - field_front_video
 * - - body
 * - - field_front_panel
 * - $node_url (string)
 * - $node (object)
 */
?>
<article id="front-page" class="node node-front node-front-full full">
  <div class="content">
    <div id="front-video">
      <div class="video-wrapper">
        <?php print !empty($content['field_front_video'][0])? render($content['field_front_video'][0]): ''; ?>
        <div id="front-page-body">
          <?php print render($content['body']); ?>
          <a href="/video/<?php print $node->nid; ?>/1/nojs" class="use-ajax">
            <div id="front-video-link">Watch Our Video</div>
          </a>
        </div>
      </div>
    </div>
    <?php print theme('weymouth_core_mobile_contact_block', _weymouth_core_get_contact_block_data()); ?>
    <?php for ($i = 0; !empty($content['field_front_panel'][$i]); $i++): ?>
    <div class="front-panel-wrapper front-panel-<?php print ($i % 2)? 'odd': 'even'; ?> front-panel-<?php print $i; ?>"<?php print ($i === 0)? ' name="case-study" id="case-study"': ''; ?>>
      <?php print render($content['field_front_panel'][$i]); ?>
    </div>
    <?php endfor; ?>
  </div>
</article>
