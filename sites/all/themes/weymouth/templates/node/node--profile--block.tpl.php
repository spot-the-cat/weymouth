<?php

/**
 * @file node--profile.tpl.php
 * Template for the block display of the profile content type.
 *
 * Available variables:
 * - $title (string)
 * - $content (render array)
 * - - body
 * - - field_profile_img
 * - - field_profile_position
 * - $node (object)
 */
?>
<article id="node-<?php print $node->nid; ?>" class="node node-profile node-profile-block node-block">
  <div class="content">
    <?php print render($content['field_profile_img']); ?><br />
    <span class="profile-time"><?php print format_date($node->created, 'time'); ?></span>
    <?php print render($content['body']); ?>
    <h2 class="title node-title"><?php print $title; ?></h2>
    <?php print render($content['field_profile_position']); ?>
  </div>
</article>



