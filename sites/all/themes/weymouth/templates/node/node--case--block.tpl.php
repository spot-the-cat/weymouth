<?php

/**
 * @file node--case.tpl.php
 * Template for the block display of the case content type.
 *
 * Available variables:
 * - $title (string)
 * - $content (render array)
 * - - body
 * - - field_case_img
 * - - field_case_panel
 * - $node (object)
 */
// We hide the comments and links now so that we can render them later.

hide($content['comments']);
hide($content['links']);
?>
<article id="node-<?php print $node->nid; ?>" class="node node-case node-case-block block">
  <h2 class="block-title"><?php print $title; ?></h2>
  <div class="content">
    <?php print render($content); ?>
    <a href="<?php print url("node/$node->nid"); ?>">
      <div class="case-view-link">
        View Case Study
      </div>
    </a>
  </div>
</article>
