<?php

/**
 * @file node--case--teaser.tpl.php
 * Template for the teaser display of the case content type.
 *
 * Available variables:
 * - $title (string)
 * - $content (render array)
 * - - body
 * - - field_case_img
 * - - field_case_panel
 * - $node (object)
 */
?>
<article id="node-<?php print $node->nid; ?>" class="node node-case node-case-teaser teaser">
  <div class="content">
    <a href="<?php print url("node/$node->nid"); ?>">
      <div class="case-img-wrapper">
        <?php print render($content['field_case_img'][0]); ?>
        <p>View Case Study</p>
      </div>
      <h2 class="title node-title"><?php print $title; ?></h2>
      <?php print render($content['body']); ?>
    </a>
  </div>
</article>
