<?php

/**
 * @file node--case.tpl.php
 * The template for the full display of the case content type.
 *
 * Available variables:
 * - $title (string)
 * - $content (render array)
 * - - body
 * - - field_case_img
 * - - field_case_panel
 * - $node (object)
 */
?>
<article id="node-<?php print $node->nid; ?>" class="node node-case node-case-full full">
  <div class="content">
    <div id="scroll-target-0">
      <?php print render($content['body']); ?>
    </div>
    
    <?php for ($i = 0, $j = 1; !empty($content['field_case_panel'][$i]); $i++, $j++): ?>
    <div id="scroll-target-<?php print $j; ?>" class="scroll-target">
      <?php print render($content['field_case_panel'][$i]); ?>
    </div>
    <?php endfor; ?>
    
  </div>
</article>

<?php if (!empty($content['field_case_panel'][0])): ?>
  <ul id="scroll-nav">
    <li id="scroll-nav-0">
      <a href="#scroll-target-0" title="<?php print $node->title; ?>"><i class="fa fa-dot-circle-o"></i></a>
    </li>
    
    <?php for ($i = 0, $j = 1; !empty($content['field_case_panel'][$i]); $i++, $j++): ?>
      <?php $panel_title = _weymouth_core_get_panel_title($content['field_case_panel'][$i]['entity']['field_collection_item'], 'field_case_panel_title'); ?>
      
      <?php if (!$panel_title): ?>
        <?php continue; ?>
      <?php endif; ?>
      
      <li id="scroll-nav-<?php print $j; ?>">
        <a href="#scroll-target-<?php print $j; ?>" title="<?php print $panel_title; ?>"><i class="fa fa-dot-circle-o"></i></a>
      </li>
    <?php endfor; ?>
    
  </ul>
<?php endif; ?>

