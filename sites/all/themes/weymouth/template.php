<?php

/**
 * @file template.php
 * Template overrides as well as (pre-)process and alter hooks for the Weymouth theme.
 */

/**
 * An alternate template render function wraps template output in HTML comments.
 */
function phptemplate_render_template($template, $vars) {
  global $conf;

  if (!empty($vars['element']['#field_name']) && $vars['element']['#field_name'] == 'field_front_panel_img') {
    return theme_render_template($template, $vars);
  }
  if (!empty($vars['element']['#field_name']) && $vars['element']['#field_name'] == 'field_front_video_img') {
    return theme_render_template($template, $vars);
  }
  if (!empty($conf['core_devel'])) {
    return "\n<!-- BEGIN $template -->\n" . theme_render_template($template, $vars) . "\n<!-- END $template -->\n";
  }
  return theme_render_template($template, $vars);
}
/**
 * Override theme_link() to force external links to be opened in a new window.
 *
 * @param $vars (array) An associative array containing the keys 'text', 'path', and 'options'. See the l() function for information about these variables.
 *
 * @return (string) An HTML link.
 */
function weymouth_link($vars) {
  if (strpos($vars['path'], 'http') === 0) {
    $vars['options']['attributes']['target'] = '_blank';
  }
  return '<a href="' . check_plain(url($vars['path'], $vars['options'])) . '"' . drupal_attributes($vars['options']['attributes']) . '>' . (!empty($vars['options']['html'])? $vars['text']: check_plain($vars['text'])) . '</a>';
}
/**
 * Implements template_preprocess_entity().
 */
function weymouth_preprocess_entity(&$vars) {
  switch ($vars['elements']['#bundle']) {
    case 'field_case_panel':
      _weymouth_add_tpl($vars, 'field_collection_item__field_case_panel');

      if (!empty($vars['content']['field_tax_dongle']['#items'])) {
        foreach ($vars['content']['field_tax_dongle']['#items'] as $i => &$item) {
          if (strtolower($item['taxonomy_term']->name) == 'slideshow') {
            $vars['content']['field_tax_dongle'][$i]['#markup'] = 'slideshow';
          }
          else {
            $img = !empty($item['taxonomy_term']->field_tax_dongle_img[LANGUAGE_NONE][0])? $item['taxonomy_term']->field_tax_dongle_img[LANGUAGE_NONE][0]: array();
            _weymouth_core_build_img($img, 'original');
            $vars['content']['field_tax_dongle'][$i]['#markup'] = $img['tag'];
          }
        }
      }
      break;

    case 'field_front_panel':
      _weymouth_add_tpl($vars, 'field_collection_item__field_front_panel');
      break;

    case 'field_front_video':
      _weymouth_add_tpl($vars, 'field_collection_item__field_front_video');
      break;
  }
}
/**
 * Implements template_preprocess_field().
 */
function weymouth_preprocess_field(&$vars) {
  switch ($vars['element']['#field_name']) {
    case 'field_front_panel_img':
      if (!empty($vars['items'][0]['#item'])) {
        $img = $vars['items'][0]['#item'];
        _weymouth_core_build_img($img, 'original');

        $vars['items'] = array(
          0 => array('#markup' => $img['url']),
        );
      }
      break;

    case 'field_front_video_img':
      if (!empty($vars['items'][0]['#item'])) {
        $img = $vars['items'][0]['#item'];
        _weymouth_core_build_img($img, 'original');

        $vars['items'] = array(
          0 => array('#markup' => $img['url']),
        );
      }
      break;

    case 'field_project_img':
      if (!empty($vars['items'][0]['#item'])) {
        $img = $vars['items'][0]['#item'];
        _weymouth_core_build_img($img, 'large');

        $vars['items'] = array(
          0 => array('#markup' => $img['url']),
        );
      }
      break;
  }
}
/**
 * Implements template_preprocess_html().
 */
function weymouth_preprocess_html(&$vars) {
  $node = menu_get_object();

// Add Javascript to every page.

  _weymouth_js();

// Add the flexslider javascript to any pages with a slideshow.

  if (!empty($vars['is_front'])) {
    _weymouth_front_page_js();
    _weymouth_scroll_nav_js();
    return drupal_add_js('misc/ajax.js');
  }
  elseif (arg(0) === 'client') {
    $options = (arg(2) == variable_get('weymouth_core_tid_video', 0))? array('slideshow' => FALSE): array();
    _weymouth_slideshow_js($options);
  }
  elseif (!empty($node->type) && $node->type === 'case') {
    _weymouth_scroll_nav_js();
    _weymouth_slideshow_js(array('slideshow' => FALSE));
  }
  if (!empty($node->nid) && db_query_range("SELECT 1 FROM {url_alias} WHERE alias = 'about' AND source = :source", 0, 1, array(':source' => "node/$node->nid"))->fetchField()) {
    drupal_add_js(drupal_get_path('theme', 'weymouth') . '/js/jquery.2.1.3.min.js');
    drupal_add_js(drupal_get_path('theme', 'weymouth') . '/js/isotope.pkgd.js');
    drupal_add_js(drupal_get_path('theme', 'weymouth') . '/js/jquery-noconflict.js');
    drupal_add_js(drupal_get_path('theme', 'weymouth') . '/js/isotope_filter.js');
  }
// Add a page specific class.

  if (arg(0) === 'client' && arg(2) == variable_get('weymouth_core_tid_video', 0)) {
    $vars['classes_array'][] = 'video-slideshow';
  }
  if (!empty($node->nid) && db_query_range("SELECT 1 FROM {url_alias} WHERE alias = 'about' AND source = :source", 0, 1, array(':source' => "node/$node->nid"))->fetchField()) {
    if (!in_array('page-about', $vars['classes_array'])) {
      $vars['classes_array'][] = 'page-about';
    }
  }
  elseif (!empty($node->nid) && db_query_range("SELECT 1 FROM {url_alias} WHERE alias = 'contact' AND source = :source", 0, 1, array(':source' => "node/$node->nid"))->fetchField()) {
    if (!in_array('page-contact', $vars['classes_array'])) {
      $vars['classes_array'][] = 'page-contact';
    }
  }
  elseif (!empty($node->nid) && db_query_range("SELECT 1 FROM {url_alias} WHERE alias = 'capabilities' AND source = :source", 0, 1, array(':source' => "node/$node->nid"))->fetchField()) {
    $vars['classes_array'][] = 'page-capabilities';
  }
  elseif (arg(0) === 'industries' && is_numeric(arg(1)) && !arg(2)) {
    if (!in_array('page-industry', $vars['classes_array'])) {
      $vars['classes_array'][] = 'page-industry';
    }
    _weymouth_core_unset_val('page-industries', $vars['classes_array']);
  }
  elseif (arg(0) === 'services' && !arg(1)) {
    if (!in_array('page-service-list', $vars['classes_array'])) {
      $vars['classes_array'][] = 'page-service-list';
    }
    _weymouth_core_unset_val('page-services', $vars['classes_array']);
  }
// Add sidebar classes.

  if (!empty($vars['page']['sidebar_first'])) {
    $vars['classes_array'][] = 'has-sidebar-first';
  }
  if (!empty($vars['page']['sidebar_second'])) {
    $vars['classes_array'][] = 'has-sidebar-second';
  }
  if (!empty($vars['page']['sidebar_second']) && !empty($vars['page']['sidebar_first'])) {
    $vars['classes_array'][] = 'has-two-sidebars';
  }
}
/**
 * Implements hook_preprocess_node().
 */
function weymouth_preprocess_node(&$vars) {

// Theme hook suggestions for node types and view modes.

  if ($vars['view_mode'] !== 'full') {
    _weymouth_add_tpl($vars, "node__{$vars['type']}__{$vars['view_mode']}");
  }
// Each webform gets it's own template, (this should probably come after other suggestions).

  if ($vars['type'] === 'webform') {
    _weymouth_add_tpl($vars, 'node__webform__' . _weymouth_core_stroked($vars['title']) . "__{$vars['view_mode']}");
  }
}
/**
 * Implements template_preprocess_page().
 */
function weymouth_preprocess_page(&$vars) {

// Make the page content type's image a background image.

  if (!empty($vars['node']->field_page_img[LANGUAGE_NONE][0]['uri'])) {
    $vars['background_img'] = file_create_url($vars['node']->field_page_img[LANGUAGE_NONE][0]['uri']);
  }
// Suppress the title on some pages.

  if (request_path() === 'about') {
    drupal_set_title('');
  }
  elseif (request_path() === 'capabilities') {
    drupal_set_title('');
  }
  elseif (!empty($vars['node']->type) && $vars['node']->type === 'project') {
    drupal_set_title('');
  }
// Add a custom breadcrumb and links to the project content type.

  if (!empty($vars['node']->type) && $vars['node']->type === 'project' && !empty($vars['node']->field_tax_market[LANGUAGE_NONE][0]['taxonomy_term'])) {
    $term = $vars['node']->field_tax_market[LANGUAGE_NONE][0]['taxonomy_term'];

    $breadcrumb = array(
      theme('weymouth_core_icon_align_justify', array('path' => 'industries')),
      l(t($term->name), "industries/$term->tid"),
      t($vars['node']->title),
    );
    drupal_set_breadcrumb($breadcrumb);

    $links = array(
      array(
        'title' => '<img src="/' . path_to_theme() . '/images/client-eye.png" width="40" height="30">',
        'href'  => "node/{$vars['node']->nid}",
        'html'  => TRUE,
      ),
    );
    if (!empty($vars['node']->field_tax_service[LANGUAGE_NONE])) {
      foreach ($vars['node']->field_tax_service[LANGUAGE_NONE] as $term) {
        $links[] = array(
          'title' => $term['taxonomy_term']->name,
          'href'  => "client/{$vars['node']->nid}/{$term['taxonomy_term']->tid}",

        );
      }
    }
    $vars['page']['content']['system_main']['nodes'][$vars['node']->nid]['links']['node']['#links'] = $links;
  }
// Turn off the single "Home" breadcrumb.

  $breadcrumb = drupal_get_breadcrumb();

  if (count($breadcrumb) === 1 && strpos($breadcrumb[0], t('Home'))) {
    drupal_set_breadcrumb(array());
  }
}
/**
 * Add a theme hook suggestion to both ends. We shouldn't have to do both, but some content types require it.
 */
function _weymouth_add_tpl(&$vars, $tpl) {
  array_unshift($vars['theme_hook_suggestions'], $tpl);
  $vars['theme_hook_suggestions'][] = $tpl;
}
/**
 * Add some front page specific JavaScript to mung the case-study top margin, set the global-menu min-height and hide the global-menu on scroll.
 */
function _weymouth_front_page_js() {
  $mobile_width = 695;
  $phone_width = 475;

// Set the case-study top margin on load.

  $js = "
    jQuery(document).ready(function() {
      if (jQuery('#front-video').width() > $mobile_width) {
        var fudge = (jQuery('body').hasClass('logged-in'))? 0: 120;
        jQuery('#case-study').css('margin-top', (jQuery('#front-video').height() + fudge) + 'px');
      }
      else if (jQuery('#front-video').width() > $phone_width) {
        var height = jQuery('#front-video').width() * .5;
        jQuery('#mobile-contact').css('margin-top', '-' + height + 'px');
      }
      else {
        jQuery('#mobile-contact').css('margin-top', '-420px');
      }
    });
  ";
// Reset the case-study top margin on resize.

  $js .= "
    jQuery(window).resize(function(e) {
      if (jQuery('#front-video').width() < $mobile_width) {
        jQuery('#case-study').css('margin-top', '5rem');
      }
      else {
        var fudge = (jQuery('body').hasClass('logged-in'))? -100: 0;
        jQuery('#case-study').css('margin-top', (jQuery('#front-video').height() + fudge) + 'px');
      }
    });
  ";
  return drupal_add_js($js, 'inline');
}
/**
 * Menu display functions.
 */
function _weymouth_js() {
  drupal_add_js(drupal_get_path('theme', 'weymouth') . '/js/jquery.session.js');
  drupal_add_js(drupal_get_path('theme', 'weymouth') . '/js/scroll_up_drop_down.js');

// Count the pages to determine if we should display the menu.

  $js = "
    jQuery(document).ready(function() {
      jQuery(function() {
        if (parseInt(jQuery.session.get('page_count')) > 0) {
          jQuery.session.set('page_count', (parseInt(jQuery.session.get('page_count')) + 1));
        }
        else {
          jQuery.session.set('page_count', 1);
          jQuery('#off-canvas').addClass('is-visible');
        }
      });
    });
  ";  
// Control the display of the menu burger and position the menu in the window.

  $js .= "
    jQuery(window).scroll(function() {
      jQuery('#global-menu').css('margin-top', jQuery(window).scrollTop() + 'px');
    });
  ";
// Make the menu container extend the full length of the page.

  $js .= "
    jQuery(document).ready(function() {
      jQuery('.l-region--navigation').css('height', (jQuery('body').height() * 2) + 'px');
    });
  ";
// Hide the menu on click.

  $js .= "
    jQuery(function() {
      jQuery('#off-canvas-menu').click(function() {
        jQuery('#off-canvas').addClass('is-visible');
        jQuery('.nav-drawer').removeClass('show');
        return false;
      });
    });
  ";
  return drupal_add_js($js, 'inline');
}
/**
 * Animate the scroll nav.
 */
function _weymouth_scroll_nav_js() {

// Animate the scroll nav.

  $js = "
    jQuery(function() {
      jQuery('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = jQuery(this.hash);

          if (this.hash.slice(1) == 'off-canvas') {
            return true;
          }
          target = target.length? target: jQuery('[name=' + this.hash.slice(1) +']');

          if (target.length) {
            jQuery('html,body').animate({scrollTop: target.offset().top - 50}, 1000);
            return false;
          }
        }
      });
    });
  ";
// Color code the scroll nav according to position.

  $js .= "
    jQuery(window).scroll(function() {
      var color_off = '#536870';
      var color_on = '#e84f3e';
      var win_pos = jQuery(window).scrollTop();

      for (i = 0; jQuery('#scroll-target-' + i).length > 0; i++) {
        var pane_pos = jQuery('#scroll-target-' + i).offset().top + (jQuery('#scroll-target-' + i).height()/2);

        if (pane_pos >= win_pos && jQuery('#scroll-nav-' + i).length > 0) {
          jQuery('#scroll-nav a').css('color', color_off);
          jQuery('#scroll-nav-' + i + ' a').css('color', color_on);
          break;
        }
      }
    });
  ";
  return drupal_add_js($js, 'inline');
}
/**
 * Add the slideshow JavaScript to a page.
 */
function _weymouth_slideshow_js($options = array()) {
  $defaults = array(
    'animation'      => 'slide',      // fade, slide
    'animationSpeed' => 1500,
    'direction'      => 'horizontal', // horizontal, vertical
    'pauseOnHover'   => TRUE,
    'slideshow'      => TRUE,
    'slideshowSpeed' => 3000,
  );
// Add the flexslider library setings and function call to the page.

  flexslider_add();
  drupal_add_js(array('weymouth' => array('options' => $options + $defaults)), 'setting');
  drupal_add_js('jQuery(document).ready(function(){jQuery(".flexslider").flexslider(Drupal.settings.weymouth.options);});', 'inline');
}
