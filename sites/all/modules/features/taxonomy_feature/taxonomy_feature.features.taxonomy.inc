<?php
/**
 * @file
 * taxonomy_feature.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function taxonomy_feature_taxonomy_default_vocabularies() {
  return array(
    'dongle' => array(
      'name' => 'Dongle',
      'machine_name' => 'dongle',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'market' => array(
      'name' => 'Market',
      'machine_name' => 'market',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'service' => array(
      'name' => 'Service',
      'machine_name' => 'service',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
