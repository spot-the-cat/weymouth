<?php
/**
 * @file
 * menu_feature.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function menu_feature_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_about:node/7
  $menu_links['main-menu_about:node/7'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/7',
    'router_path' => 'node/%',
    'link_title' => 'About',
    'options' => array(
      'identifier' => 'main-menu_about:node/7',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 5,
    'customized' => 0,
  );
  // Exported menu link: main-menu_blog:http://blog.weymouthdesign.com/
  $menu_links['main-menu_blog:http://blog.weymouthdesign.com/'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'http://blog.weymouthdesign.com/',
    'router_path' => '',
    'link_title' => 'Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_blog:http://blog.weymouthdesign.com/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 8,
    'customized' => 1,
  );
  // Exported menu link: main-menu_capabilities:node/5
  $menu_links['main-menu_capabilities:node/5'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/5',
    'router_path' => 'node/%',
    'link_title' => 'Capabilities',
    'options' => array(
      'identifier' => 'main-menu_capabilities:node/5',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 6,
    'customized' => 0,
  );
  // Exported menu link: main-menu_case-studies:case-studies
  $menu_links['main-menu_case-studies:case-studies'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'case-studies',
    'router_path' => 'case-studies',
    'link_title' => 'Case-Studies',
    'options' => array(
      'identifier' => 'main-menu_case-studies:case-studies',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 0,
  );
  // Exported menu link: main-menu_consumer:industries/3
  $menu_links['main-menu_consumer:industries/3'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'industries/3',
    'router_path' => 'industries/3',
    'link_title' => 'Consumer',
    'options' => array(
      'identifier' => 'main-menu_consumer:industries/3',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_who-we-do-it-for:industries',
  );
  // Exported menu link: main-menu_contact:node/3
  $menu_links['main-menu_contact:node/3'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/3',
    'router_path' => 'node/%',
    'link_title' => 'Contact',
    'options' => array(
      'identifier' => 'main-menu_contact:node/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 7,
    'customized' => 0,
  );
  // Exported menu link: main-menu_education:industries/4
  $menu_links['main-menu_education:industries/4'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'industries/4',
    'router_path' => 'industries/4',
    'link_title' => 'Education',
    'options' => array(
      'identifier' => 'main-menu_education:industries/4',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 0,
    'parent_identifier' => 'main-menu_who-we-do-it-for:industries',
  );
  // Exported menu link: main-menu_financial:industries/5
  $menu_links['main-menu_financial:industries/5'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'industries/5',
    'router_path' => 'industries/5',
    'link_title' => 'Financial',
    'options' => array(
      'identifier' => 'main-menu_financial:industries/5',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 0,
    'parent_identifier' => 'main-menu_who-we-do-it-for:industries',
  );
  // Exported menu link: main-menu_healthcare:industries/6
  $menu_links['main-menu_healthcare:industries/6'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'industries/6',
    'router_path' => 'industries/6',
    'link_title' => 'Healthcare',
    'options' => array(
      'identifier' => 'main-menu_healthcare:industries/6',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 0,
    'parent_identifier' => 'main-menu_who-we-do-it-for:industries',
  );
  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: main-menu_identity:services/8
  $menu_links['main-menu_identity:services/8'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'services/8',
    'router_path' => 'services/8',
    'link_title' => 'Identity',
    'options' => array(
      'identifier' => 'main-menu_identity:services/8',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 5,
    'customized' => 0,
    'parent_identifier' => 'main-menu_what-we-do:services',
  );
  // Exported menu link: main-menu_mobile:services/33
  $menu_links['main-menu_mobile:services/33'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'services/33',
    'router_path' => 'services/33',
    'link_title' => 'Mobile',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_mobile:services/33',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 6,
    'customized' => 1,
    'parent_identifier' => 'main-menu_what-we-do:services',
  );
  // Exported menu link: main-menu_photo:services/9
  $menu_links['main-menu_photo:services/9'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'services/9',
    'router_path' => 'services/9',
    'link_title' => 'Photo',
    'options' => array(
      'identifier' => 'main-menu_photo:services/9',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 7,
    'customized' => 0,
    'parent_identifier' => 'main-menu_what-we-do:services',
  );
  // Exported menu link: main-menu_print:services/10
  $menu_links['main-menu_print:services/10'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'services/10',
    'router_path' => 'services/10',
    'link_title' => 'Print',
    'options' => array(
      'identifier' => 'main-menu_print:services/10',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 8,
    'customized' => 0,
    'parent_identifier' => 'main-menu_what-we-do:services',
  );
  // Exported menu link: main-menu_tablet:services/34
  $menu_links['main-menu_tablet:services/34'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'services/34',
    'router_path' => 'services/34',
    'link_title' => 'Tablet',
    'options' => array(
      'identifier' => 'main-menu_tablet:services/34',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 9,
    'customized' => 1,
    'parent_identifier' => 'main-menu_what-we-do:services',
  );
  // Exported menu link: main-menu_technology:industries/7
  $menu_links['main-menu_technology:industries/7'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'industries/7',
    'router_path' => 'industries/7',
    'link_title' => 'Technology',
    'options' => array(
      'identifier' => 'main-menu_technology:industries/7',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 0,
    'parent_identifier' => 'main-menu_who-we-do-it-for:industries',
  );
  // Exported menu link: main-menu_video:services/11
  $menu_links['main-menu_video:services/11'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'services/11',
    'router_path' => 'services/11',
    'link_title' => 'Video',
    'options' => array(
      'identifier' => 'main-menu_video:services/11',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 10,
    'customized' => 0,
    'parent_identifier' => 'main-menu_what-we-do:services',
  );
  // Exported menu link: main-menu_web:services/12
  $menu_links['main-menu_web:services/12'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'services/12',
    'router_path' => 'services/12',
    'link_title' => 'Web',
    'options' => array(
      'identifier' => 'main-menu_web:services/12',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 11,
    'customized' => 0,
    'parent_identifier' => 'main-menu_what-we-do:services',
  );
  // Exported menu link: main-menu_what-we-do:services
  $menu_links['main-menu_what-we-do:services'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'services',
    'router_path' => 'services',
    'link_title' => 'What We Do',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_what-we-do:services',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 2,
    'customized' => 1,
  );
  // Exported menu link: main-menu_who-we-do-it-for:industries
  $menu_links['main-menu_who-we-do-it-for:industries'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'industries',
    'router_path' => 'industries',
    'link_title' => 'Who we do it for',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_who-we-do-it-for:industries',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 3,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About');
  t('Blog');
  t('Capabilities');
  t('Case-Studies');
  t('Consumer');
  t('Contact');
  t('Education');
  t('Financial');
  t('Healthcare');
  t('Home');
  t('Identity');
  t('Mobile');
  t('Photo');
  t('Print');
  t('Tablet');
  t('Technology');
  t('Video');
  t('Web');
  t('What We Do');
  t('Who we do it for');


  return $menu_links;
}
