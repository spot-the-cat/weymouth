<?php
/**
 * @file
 * developer_feature.default_environment_indicator_environments.inc
 */

/**
 * Implements hook_default_environment_indicator_environment().
 */
function developer_feature_default_environment_indicator_environment() {
  $export = array();

  $environment = new stdClass();
  $environment->disabled = FALSE; /* Edit this to true to make a default environment disabled initially */
  $environment->api_version = 1;
  $environment->machine = 'staging';
  $environment->name = 'Staging';
  $environment->regexurl = 'dev.weymouth.idea.weymouthdesign.com';
  $environment->settings = array(
    'color' => '#e9e73f',
    'text_color' => '#0f100e',
    'weight' => '',
    'position' => 'top',
    'fixed' => 1,
  );
  $export['staging'] = $environment;

  $environment = new stdClass();
  $environment->disabled = FALSE; /* Edit this to true to make a default environment disabled initially */
  $environment->api_version = 1;
  $environment->machine = 'vbox';
  $environment->name = 'VBox';
  $environment->regexurl = 'weymouth.vbx';
  $environment->settings = array(
    'color' => '#448c3f',
    'text_color' => '#ffffff',
    'weight' => '',
    'position' => 'top',
    'fixed' => 0,
  );
  $export['vbox'] = $environment;

  return $export;
}
