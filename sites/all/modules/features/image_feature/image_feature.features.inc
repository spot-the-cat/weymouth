<?php
/**
 * @file
 * image_feature.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function image_feature_image_default_styles() {
  $styles = array();

  // Exported image style: small.
  $styles['small'] = array(
    'label' => 'Small (250x150)',
    'effects' => array(
      6 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 250,
          'height' => 150,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
