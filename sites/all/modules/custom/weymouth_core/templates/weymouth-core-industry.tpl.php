<?php

/**
 * @file weymouth-core-industry.tpl.php
 * The theme template of for an industry page.
 *
 * Available variables:
 * - $data (object)
 * - - title (string)
 * - - term (string)
 * - - nid (int)
 * - - tid (int)
 * - - terms (array)
 * - - slides (array of objects)
 * - - - type (string)
 * - - - value (string)
 * - - - data (object) 
 */
?>
<div id="industry">
  <ul id="client-services">
    
  <?php foreach ($data->terms as $tid => $term): ?>
    <li id="client-service-<?php print $tid; ?>"><?php print l($term, "client/$data->nid/$tid"); ?></li>
  <?php endforeach; ?>
  
  </ul>
  <div class="content">
    
  <?php foreach ($data->slides as $obj): ?>
    <div id="industry-<?php print $obj->type; ?>">
      <?php print $obj->value; ?>
    </div>
  <?php endforeach; ?>

  </div>
</div>
