<?php

/**
 * @file weymouth-core-clients-block.tpl.php
 * Template for the clients block.
 *
 * Available variables:
 * - $data (object)
 * - - clients (array of objects)
 * - - - nid
 * - - - title
 * - - - class
 * - - - img (object)
 * - - - - width (int)
 * - - - - height (int)
 * - - - - url (string)
 * - - - - tag (string)
 * - - nav (hash)
 * - - - class => title
 */
?>
<h1>We've Worked With&hellip;</h1>
<div class="button-group filters-button-group">
  <button class="button is-checked" data-filter="*">All</button>

  <?php foreach ($data->nav as $nav_class => $nav_title): ?>
    <button class="button" data-filter="<?php print $nav_class; ?>"><?php print $nav_title; ?></button>
  <?php endforeach; ?>

</div>
<div class="grid">

<?php foreach ($data->clients as $obj): ?>
  <div class="element-item <?php print $obj->class; ?>" data-category="">
    <a href="<?php print url("node/$obj->nid"); ?>">
      <?php print $obj->img->tag; ?>
    </a>
  </div>
<?php endforeach; ?>

</div>
<div class="clients-contact">
  <p><a href="/contact"><strong>Like Us?</strong> Let's Plan the Next Great Thing</a> <i class="fa fa-arrow-circle-o-right"></i></p>
</div>
