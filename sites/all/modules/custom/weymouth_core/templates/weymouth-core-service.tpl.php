<?php

/**
 * @file weymouth-core-service.tpl.php
 * The theme template of for a service list page.
 *
 * Available variables:
 * - $data (array of objects)
 * - - title (string)
 * - - term (string)
 * - - nid (int)
 * - - tid (int)
 * - - img (object)
 * - - - fid (int)
 * - - - filepath (string)
 * - - - height (int)
 * - - - style_uri (string)
 * - - - tag (string)
 * - - - uri (string)
 * - - - url (string)
 * - - - width (int)
 */
?>
<ul id="project-service">
  
<?php foreach ($data as $obj): ?>
  <li>
    <a href="<?php print url("client/$obj->nid/$obj->tid"); ?>">
      <div class="project-service-item">
          <?php print $obj->img->tag; ?>
          <h2><?php print $obj->title; ?></h2>
      </div>
    </a>
  </li>
<?php endforeach; ?>

</ul>
