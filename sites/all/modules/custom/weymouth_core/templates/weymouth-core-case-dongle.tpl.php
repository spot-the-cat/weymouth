<?php

/**
 * @file weymouth-core-case-dongle.tpl.php
 * Template to display the image or video fields in the case_panel field collection seperated by the selected dongle.
 *
 * Available variables:
 * - $content (render array)
 * - - field_case_panel_fluf (array)
 * - - field_tax_dongle (string)
 */
?>
<?php for ($i = 0; !empty($content['field_case_panel_fluf'][$i]); $i++): ?>
  <div class="case-panel-imgs">
    <div id="case-panel-img-<?php print $i; ?>" class="case-panel-img">
      <?php print render($content['field_case_panel_fluf'][$i]); ?>
    </div>

  <?php if (!empty($content['field_case_panel_fluf'][$i+1])): ?>
    <div class="case-panel-dongle">
      <?php print render($content['field_tax_dongle']); ?>
    </div>
  <?php endif; ?>

  </div>
<?php endfor; ?>

