<?php

/**
 * @file weymouth-core-market.tpl.php
 * The theme template of for a industries list page.
 *
 * Available variables:
 * - $data (array of objects)
 * - - title (string)
 * - - term (string)
 * - - nid (int)
 * - - tid (int)
 * - - content (string)
 */
?>
<ul id="project-market">
  
<?php foreach ($data as $obj): ?>
  <li>
    <a href="<?php print url("node/$obj->nid"); ?>">
      <?php print $obj->content; ?>
    </a>
  </li>
<?php endforeach; ?>

</ul>
