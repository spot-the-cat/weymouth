<?php

/**
 * @file weymouth-core-icon-align-justify.tpl.php
 * Template for the top level breadcrumb icon.
 *
 * Available variables:
 * - $path (string)
 */
?>
<a href="/<?php print $path; ?>" class="crumb-icon"><i class="fa fa-align-justify"></i></a>
