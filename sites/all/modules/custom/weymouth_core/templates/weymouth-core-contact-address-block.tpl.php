<?php

/**
 * @file weymouth-core-contact-address.tpl.php
 * Template for the contact address block.
 *
 * Available variables:
 * - $data (array of objects)
 * - - title (string)
 * - - body (string)
 * - - id (string)
 * - - map (string)
 * - - street (string)
 * - - additional (string)
 * - - city (string)
 * - - state (string)
 * - - zip (string)
 * - - email (string)
 * - - phone (string)
 * - - img (object)
 * - - - height (string)
 * - - - width (string)
 * - - - tag (string)
 * - - - url (string)
 */
?>
<div id="contact-address">

<?php foreach ($data as $obj): ?>
  <div id="contact-office-<?php print $obj->id; ?>" class="contact-office">
    <img src="<?php print $obj->img->url; ?>">
    <div class="address contact-office-address">
      <div class="contact-address-body">
        <?php print $obj->body; ?>
      </div>
      <?php print $obj->street; ?><?php print !empty($obj->additional)? ", $obj->additional": ''; ?><?php print !empty($obj->city)? ", $obj->city": ''; ?><?php print !empty($obj->state)? ", $obj->state": ''; ?> <?php print $obj->zip; ?><br />
      <?php print $obj->phone; ?><br />
      <?php print l($obj->email, "mailto:$obj->email"); ?><br />
      <a href="<?php print $obj->map; ?>" target="_blank">Get Directions</a>
    </div>
  </div>
<?php endforeach; ?>

</div>
