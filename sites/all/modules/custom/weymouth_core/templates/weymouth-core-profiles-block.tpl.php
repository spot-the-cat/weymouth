<?php

/**
 * @file weymouth-core-profiles-block.tpl.php
 * Template for the profiles block.
 *
 * Available variables:
 * - $data (array of objects)
 * - - nid (int)
 * - - node (string)
 */
?>
<ul id="profiles-block">

<?php foreach ($data as $obj): ?>
  <li id="profile-<?php print $obj->nid; ?>" class="profile-block">
    <?php print $obj->node; ?>
  </li>
<?php endforeach; ?>

</ul>
