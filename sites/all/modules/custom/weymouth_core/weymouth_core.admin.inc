<?php

/**
 * @file
 * The weymouth_core module's settings.
 */

/**
 * The admin settings form.
 */
function weymouth_core_settings_form($form_id, &$form_state) { 
  $form['weymouth_core_blog_summary_length'] = array(
    '#title'         => t('Blog Summary Length'),
    '#type'          => 'select',
    '#options'       => array_combine(range(0, 600, 10), range(0, 600, 10)),
    '#default_value' => variable_get('weymouth_core_blog_summary_length', 150),
    '#required'      => TRUE,
  );
  $form['weymouth_core_blog_url'] = array(
    '#title'         => t('Weymouth Design Blog URL'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('weymouth_core_blog_url', ''),
    '#required'      => TRUE,
  );
  $form['weymouth_core_facebook_url'] = array(
    '#title'         => t('Facebook URL'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('weymouth_core_facebook_url', ''),
    '#required'      => TRUE,
  );
  $form['weymouth_core_google_map_url'] = array(
    '#title'         => t('Google Maps URL'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('weymouth_core_google_map_url', ''),
    '#required'      => TRUE,
  );
  $form['weymouth_core_linkedin_url'] = array(
    '#title'         => t('Linkedin URL'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('weymouth_core_linkedin_url', ''),
    '#required'      => TRUE,
  );
  $form['weymouth_core_twitter_url'] = array(
    '#title'         => t('Twitter URL'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('weymouth_core_twitter_url', ''),
    '#required'      => TRUE,
  );
  $form['weymouth_core_vimeo_url'] = array(
    '#title'         => t('Vimeo URL'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('weymouth_core_vimeo_url', ''),
    '#required'      => TRUE,
  );
  $form['blog_header'] = array(
    '#title'       => t('Blog Header'),
    '#description' => t('The content at the top of the home page blog list.'),
    '#type'        => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  $form['blog_header']['weymouth_core_block_blog_content'] = array(
    '#type'          => 'text_format',
    '#default_value' => _weymouth_core_get_block(variable_get('weymouth_core_block_blog', 0)),
    '#format'        => _weymouth_core_get_block_format(variable_get('weymouth_core_block_blog', 0)),
    '#required'      => FALSE,
  );
  $form['#submit'][] = 'weymouth_core_settings_submit';
  
  return system_settings_form($form);
}
/**
 * Custom submit hook.
 */
function weymouth_core_settings_submit($form, &$form_state) {

// Update the careers content.

  $bid = variable_get('weymouth_core_block_blog', 0);
  $fields = array(
    'body'   => $form_state['values']['weymouth_core_block_blog_content']['value'],
    'format' => $form_state['values']['weymouth_core_block_blog_content']['format'],
  );
  db_update('block_custom')->fields($fields)->condition('bid', $bid)->execute();

  unset($form_state['values']['weymouth_core_block_blog_content']);
}
