<?php
/**
 * @file
 * content_project_feature.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function content_project_feature_taxonomy_default_vocabularies() {
  return array(
    'market' => array(
      'name' => 'Market',
      'machine_name' => 'market',
      'description' => '<p>Lorem ipsum dolor sit bacon, consectetur adipisicing elit, sed do eiusmod tempor bacon ut labore et dolore magna aliqua.</p>
<p>Ut enim ad minim veniam, quis bacon exercitation ullamco laboris nisi ut bacon ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint bacon cupidatat non proident, sunt in culpa qui officia bacon mollit anim id est bacon.</p>',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
