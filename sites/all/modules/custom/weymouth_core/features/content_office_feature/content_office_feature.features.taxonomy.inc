<?php
/**
 * @file
 * content_office_feature.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function content_office_feature_taxonomy_default_vocabularies() {
  return array(
    'service' => array(
      'name' => 'Service',
      'machine_name' => 'service',
      'description' => NULL,
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
